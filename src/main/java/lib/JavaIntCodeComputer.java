package lib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JavaIntCodeComputer {

    private int instructionPointer = 0;
    private int relativeBase = 0;
    private boolean done = false;

    private Memory memory;
    private List<Long> input = new ArrayList<>();
    private List<Long> output = new ArrayList<>();

    private static final String POSITION = "0";
    private static final String IMMEDIATE = "1";
    private static final String RELATIVE = "2";

    public JavaIntCodeComputer(List<Long> program) {
        this.memory = new Memory(program);
    }

    public void run() {
        while (!done) {
            doCalculation();
        }
    }

    public List<Long> getOutput() {
        return output;
    }

    public long getResult() {
        return memory.get(0);
    }

    public void dumpMemory() {
        System.out.println(memory);
    }

    private void doCalculation() {
        Instruction instruction = new Instruction(memory.get(instructionPointer));

        switch (instruction.opCode()) {
            case ADD: {
                long result = read(instruction, 1) + read(instruction, 2);
                int out = readAddress(instruction, 3);
                memory.set(out, result);
                instructionPointer += 4;
                break;
            }
            case MULT: {
                long result = read(instruction, 1) * read(instruction, 2);
                int out = readAddress(instruction, 3);
                memory.set(out, result);
                instructionPointer += 4;
                break;
            }
            case STOR: {
                int out = readAddress(instruction, 1);
                long value = input.remove(0);
                memory.set(out, value);
                instructionPointer += 2;
                break;
            }
            case READ: {
                output.add(read(instruction, 1));
                instructionPointer += 2;
                break;
            }
            case JUMP_IF_TRUE: {
                long left = read(instruction, 1);
                long right = read(instruction, 2);
                if (left != 0L) {
                    instructionPointer = (int) right;
                } else {
                    instructionPointer += 3;
                }
                break;
            }
            case JUMP_IF_FALSE: {
                long left = read(instruction, 1);
                long right = read(instruction, 2);
                if (left == 0L) {
                    instructionPointer = (int) right;
                } else {
                    instructionPointer += 3;
                }
                break;
            }
            case LT: {
                long left = read(instruction, 1);
                long right = read(instruction, 2);
                int out = readAddress(instruction, 3);
                if (left < right) {
                    memory.set(out, 1L);
                } else {
                    memory.set(out, 0L);
                }
                instructionPointer += 4;
                break;
            }
            case EQ: {
                long left = read(instruction, 1);
                long right = read(instruction, 2);
                int out = readAddress(instruction, 3);
                if (left == right) {
                    memory.set(out, 1L);
                } else {
                    memory.set(out, 0L);
                }
                instructionPointer += 4;
                break;
            }
            case REBASE: {
                relativeBase += read(instruction, 1);
                instructionPointer += 2;
                break;
            }
            case HALT: {
                done = true;
            }
        }
    }

    private int readAddress(Instruction instruction, int offset) {
        String mode = instruction.modeFor(offset);
        if (mode.equals(IMMEDIATE) || mode.equals(POSITION)) {
            return (int) read(offset, IMMEDIATE);
        } else {
            // RELATIVE
            return relativeBase + ((int) read(offset, IMMEDIATE));
        }

    }

    private long read(int offset, String mode) {
        int readFromMemoryPointer = instructionPointer + offset;

        if (mode.equals(POSITION)) {
            return memory.get((int) memory.get(readFromMemoryPointer));
        } else if (mode.equals(RELATIVE)) {
            int relativeOffset = (int) memory.get(readFromMemoryPointer);
            return memory.get(relativeBase + relativeOffset);
        } else {
            return memory.get(readFromMemoryPointer);
        }
    }

    private long read(Instruction instruction, int offset) {
        String mode = instruction.modeFor(offset);
        return read(offset, mode);
    }

    Memory getMemory() {
        return memory;
    }

    public void setInput(long in) {
        input.add(in);
    }

    public void setInput(List<Integer> in) {
        input.clear();
        in.forEach(i -> input.add(i.longValue()));
    }

    public long runToNextOutput() {
        int currentSize = output.size();
        while (output.size() == currentSize) {
            doCalculation();
        }
        return output.get(output.size() - 1);
    }

    public boolean halted() {
        return done;
    }

    private static class Instruction {

        private final String instruction;

        Instruction(long instr) {
            instruction = leftPad(instr);
        }

        private static String leftPad(long integer) {
            String s = String.valueOf(integer);
            while (s.length() < 5) {
                s = "0" + s;
            }
            return s;
        }

        String modeFor(int offset) {
            return instruction.substring(3 - offset, 3 - offset + 1);
        }

        OpCode opCode() {
            return OpCode.of(instruction.substring(3, 5));
        }
    }

    public enum OpCode {

        ADD("01"),
        MULT("02"),
        STOR("03"),
        READ("04"),
        JUMP_IF_TRUE("05"),
        JUMP_IF_FALSE("06"),
        LT("07"),
        EQ("08"),
        REBASE("09"),
        HALT("99");

        private final String code;

        OpCode(String code) {
            this.code = code;
        }

        public static OpCode of(String code) {
            for (OpCode opCode : OpCode.values()) {
                if (opCode.code.equals(code)) {
                    return opCode;
                }
            }
            throw new IllegalArgumentException("Unknown opCode " + code);
        }
    }

    public static class Memory {
        private final Map<Integer, Long> internalMemory = new HashMap<>();

        Memory(List<Long> program) {
            for (int i = 0; i < program.size(); i++) {
                internalMemory.put(i, program.get(i));
            }
        }

        public long get(int i) {
            return internalMemory.getOrDefault(i, 0L);
        }

        void set(int address, long value) {
            internalMemory.put(address, value);
        }
    }
}
