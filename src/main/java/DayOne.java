import java.util.ArrayList;
import java.util.List;

public class DayOne {

    public static void main(String[] args) {
        partOne();
        partTwo();
    }

    private static void partOne() {
        List<Long> masses = readInput();
        Long sum = masses.stream()
                .mapToLong(DayOne::fuelRequiredFor)
                .sum();

        System.out.println(sum);
    }

    private static void partTwo() {
        List<Long> masses = readInput();
        Long sum = masses.stream()
                .mapToLong(DayOne::recursiveFuelRequiredFor)
                .sum();

        System.out.println(sum);
    }

    private static long recursiveFuelRequiredFor(long mass) {
        long f1 = fuelRequiredFor(mass);
        if (f1 > 0) {
            return f1 + recursiveFuelRequiredFor(f1);
        }
        return Math.max(0, f1);
    }

    private static long fuelRequiredFor(Long mass) {
        return (mass / 3) - 2;
    }

    private static List<Long> readInput() {
        List<Long> in = new ArrayList<Long>();
        in.add(136995L);
        in.add(113523L);
        in.add(51895L);
        in.add(79350L);
        in.add(124361L);
        in.add(62331L);
        in.add(93313L);
        in.add(67673L);
        in.add(65387L);
        in.add(139570L);
        in.add(74864L);
        in.add(73723L);
        in.add(142366L);
        in.add(108790L);
        in.add(50333L);
        in.add(109242L);
        in.add(67155L);
        in.add(126685L);
        in.add(148459L);
        in.add(126160L);
        in.add(56323L);
        in.add(123773L);
        in.add(116336L);
        in.add(123357L);
        in.add(117877L);
        in.add(90720L);
        in.add(105322L);
        in.add(92084L);
        in.add(100609L);
        in.add(143050L);
        in.add(99542L);
        in.add(137618L);
        in.add(70385L);
        in.add(116984L);
        in.add(137877L);
        in.add(142591L);
        in.add(104263L);
        in.add(77096L);
        in.add(107016L);
        in.add(106030L);
        in.add(88411L);
        in.add(56359L);
        in.add(129141L);
        in.add(88179L);
        in.add(82296L);
        in.add(66855L);
        in.add(146894L);
        in.add(65655L);
        in.add(65481L);
        in.add(107083L);
        in.add(129529L);
        in.add(94207L);
        in.add(118038L);
        in.add(93389L);
        in.add(116976L);
        in.add(141468L);
        in.add(137616L);
        in.add(78852L);
        in.add(57602L);
        in.add(82514L);
        in.add(59790L);
        in.add(115105L);
        in.add(125868L);
        in.add(104067L);
        in.add(100487L);
        in.add(109434L);
        in.add(68047L);
        in.add(84831L);
        in.add(64928L);
        in.add(131286L);
        in.add(78450L);
        in.add(70130L);
        in.add(84341L);
        in.add(105659L);
        in.add(61101L);
        in.add(137930L);
        in.add(125096L);
        in.add(73937L);
        in.add(58756L);
        in.add(123901L);
        in.add(123296L);
        in.add(110409L);
        in.add(104259L);
        in.add(87899L);
        in.add(97236L);
        in.add(111253L);
        in.add(60227L);
        in.add(129468L);
        in.add(79553L);
        in.add(55170L);
        in.add(99267L);
        in.add(101485L);
        in.add(146930L);
        in.add(105511L);
        in.add(145835L);
        in.add(98257L);
        in.add(147609L);
        in.add(143714L);
        in.add(55276L);
        in.add(66162L);
        return in;
    }

}
