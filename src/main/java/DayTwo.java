import lib.JavaIntCodeComputer;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DayTwo {

    public static void main(String[] args) {
        partOne();
        partTwo();
    }

    private static void partTwo() {
        for (long i = 0; i < 100; i++) {
            for (long j = 0; j < 100; j++) {
                List<Long> input = readInput();
                input.set(1, i);
                input.set(2, j);
                JavaIntCodeComputer computer = new JavaIntCodeComputer(input);
                computer.run();
                long result = computer.getResult();
                if (result == 19690720L) {
                    System.out.println(String.format("Hit! i=%d, j=%d, a=%d", i, j, 100 * i + j));
                }
            }
        }
    }

    private static void partOne() {
        List<Long> input = readInput();
        input.set(1, 12L);
        input.set(2, 2L);

        JavaIntCodeComputer computer = new JavaIntCodeComputer(input);
        computer.run();
        computer.dumpMemory();
        System.out.println(computer.getResult());
    }


    private static List<Long> readInput() {
        String in = "1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,6,1,19,1,19,10,23,2,13,23,27,1,5,27,31,2,6,31,35,1,6,35,39,2,39,9,43,1,5,43,47,1,13,47,51,1,10,51,55,2,55,10,59,2,10,59,63,1,9,63,67,2,67,13,71,1,71,6,75,2,6,75,79,1,5,79,83,2,83,9,87,1,6,87,91,2,91,6,95,1,95,6,99,2,99,13,103,1,6,103,107,1,2,107,111,1,111,9,0,99,2,14,0,0";
        return stringToList(in);
    }

    private static List<Long> stringToList(String in) {
        String[] split = in.split(",");
        return Arrays.stream(split)
                .map(Long::parseLong)
                .collect(Collectors.toList());
    }
}
