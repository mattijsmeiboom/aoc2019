package alt

import java.math.BigInteger
import kotlin.math.pow

fun day22a(instructions: List<String>, nrOfCards: Long = 10007, cardNrToTrack: Long = 2019): Long {
    return instructions.fold(cardNrToTrack) { index, instruction ->
        val segments = instruction.split(" ")
        when (segments[1]) {
            "with" -> {
                val increment = segments[3].toLong()
                (increment * index) % nrOfCards
            }
            "into" -> (nrOfCards - 1) - index
            else -> {
                val cutIndex = segments[1].toLong()
                (index - cutIndex) % nrOfCards
            }
        }
    }
}


fun day22b(
        instructions: List<String>,
        nrOfCards: Long = 119315717514047,
        targetIndex: Long = 2020,
        repeats: Long = 101741582076661
): Long {
    var offset = BigInteger.ZERO
    var delta = BigInteger.ONE

    instructions.forEach { instruction ->
        val segments = instruction.split(" ")
        when (segments[1]) {
            "into" -> {
                delta = (delta * -1) % nrOfCards
                offset = (offset + delta) % nrOfCards
            }
            "with" -> {
                val increment = segments[3].toInt()
                delta = (delta * increment.modInverse(nrOfCards)) % nrOfCards
            }
            else -> {
                val cutIndex = segments[1].toInt()
                offset = (offset + (delta * cutIndex)) % nrOfCards
            }

        }
    }

    val finalDelta = delta.modPow(repeats, nrOfCards)
    val finalOffset = (offset * (1 - finalDelta) * (1 - delta).modInverse(nrOfCards)) % nrOfCards
    val finalIndex = (finalOffset + finalDelta * targetIndex) % nrOfCards

    return finalIndex.toLong()
}

operator fun BigInteger.rem(m: Long): BigInteger = this.mod(BigInteger.valueOf(m))
operator fun BigInteger.times(other: Long): BigInteger = this.times(BigInteger.valueOf(other))
operator fun BigInteger.times(other: Int): BigInteger = this.times(BigInteger.valueOf(other.toLong()))
operator fun BigInteger.plus(other: Int): BigInteger = this.plus(BigInteger.valueOf(other.toLong()))
operator fun Int.minus(other: BigInteger): BigInteger = BigInteger.valueOf(this.toLong()).minus(other)
fun BigInteger.modPow(e: Long, m: Long): BigInteger = this.modPow(BigInteger.valueOf(e), BigInteger.valueOf(m))
fun BigInteger.modInverse(m: Long): BigInteger = this.modInverse(BigInteger.valueOf(m))
fun Long.modInverse(m: Long): BigInteger = BigInteger.valueOf(this).modInverse(BigInteger.valueOf(m))
fun Int.modInverse(m: Long): BigInteger = BigInteger.valueOf(this.toLong()).modInverse(BigInteger.valueOf(m))
fun Long.pow(e: Long): Long = this.toDouble().pow(e.toDouble()).toLong()