import lib.Day
import lib.JavaIntCodeComputer
import lib.resourceString

object Day5 : Day {

    override fun part1(): Any {
        val testProgram = resourceString(2019, 5).split(",").map { it.toLong() }
        val computer = JavaIntCodeComputer(testProgram)
        computer.setInput(1)
        computer.run()
        return computer.output.last()
    }

    override fun part2(): Any {
        val testProgram = resourceString(2019, 5).split(",").map { it.toLong() }
        val computer = JavaIntCodeComputer(testProgram)
        computer.setInput(5)
        computer.run()
        return computer.output.last()
    }
}