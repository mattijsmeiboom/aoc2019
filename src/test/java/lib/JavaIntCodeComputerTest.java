package lib;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class JavaIntCodeComputerTest {

    @Test
    void testMultiple() {
        List<Long> program = program("1002,4,3,4,33");
        JavaIntCodeComputer computer = new JavaIntCodeComputer(program);
        computer.run();
        assertEquals(computer.getMemory().get(4), 99L);
    }

    @Test
    void testAdd_negativeNumbers() {
        List<Long> program = program("1101,100,-1,4,0");
        JavaIntCodeComputer computer = new JavaIntCodeComputer(program);
        computer.run();
        assertEquals(computer.getMemory().get(4), 99L);
    }

    @Test
    void testEquals() {
        List<Long> program = program("3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8");
        assertOutput(program, 7, 0L);
        assertOutput(program, 8, 1L);
        assertOutput(program, 9, 0L);
    }

    @Test
    void testLessThanEight() {
        List<Long> program = program("3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8");
        assertOutput(program, 7, 1L);
        assertOutput(program, 8, 0L);
    }

    @Test
    void testEquals__usingImmediateMode() {
        List<Long> program = program("3,3,1108,-1,8,3,4,3,99");
        assertOutput(program, 7, 0L);
        assertOutput(program, 8, 1L);
        assertOutput(program, 9, 0L);
    }

    @Test
    void testInput__usingRelativeMode() {
        List<Long> program = program("109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99");
        assertOutput(program, program);
    }

    @Test
    void testLargeOutput() {
        List<Long> program = program("1102,34915192,34915192,7,4,7,99,0");
        assertOutput(program, 1219070632396864L);
    }

    @Test
    void testLargeInput() {
        List<Long> program = program("104,1125899906842624,99");
        assertOutput(program, 1125899906842624L);
    }

    @Test
    void testLessThan() {
        List<Long> program = program("3,9,7,9,10,9,4,9,99,-1,8");
        assertOutput(program, 7, 1);
    }

    private void assertOutput(List<Long> program, long expectedOutput) {
        assertOutput(program, Collections.singletonList(expectedOutput));
    }

    private void assertOutput(List<Long> program, List<Long> expectedOutput) {
        assertOutput(program, new ArrayList<>(), expectedOutput);
    }

    private void assertOutput(List<Long> program, int input, long expectedOutput) {
        assertOutput(program, Collections.singletonList(input), Collections.singletonList(expectedOutput));
    }

    private void assertOutput(List<Long> program, List<Integer> input, List<Long> expectedOutput) {
        JavaIntCodeComputer computer = new JavaIntCodeComputer(program);
        computer.setInput(input);
        computer.run();
        assertEquals(expectedOutput, computer.getOutput());
    }

    private List<Long> program(String p) {
        return Stream.of(p.split(","))
                .map(s -> Long.parseLong(s.trim()))
                .collect(Collectors.toList());
    }

}